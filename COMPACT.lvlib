﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="Alarm Database Computer" Type="Str">DEPC456</Property>
	<Property Name="Alarm Database Name" Type="Str">COMPACT</Property>
	<Property Name="Alarm Database Path" Type="Str">d:\dsc\compact</Property>
	<Property Name="Data Lifespan" Type="UInt">365</Property>
	<Property Name="Database Computer" Type="Str">DEPC456</Property>
	<Property Name="Database Name" Type="Str">COMPACT</Property>
	<Property Name="Database Path" Type="Str">d:\dsc\compact</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">6</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="COMPACT_PollingCounter" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&gt;'1!!!"=!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="COMPACT_PollingInterval" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="COMPACT_PollingIterations" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&gt;'1!!!"=!A!!!!!!"!!5!"Q!!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="COMPACT_PollingMode" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&gt;'1!!!"=!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="COMPACT_PollingTime" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Dewpoint" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str"></Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">Dewpoint is high</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">230.000000</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">Hi</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:HiHi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Area" Type="Str"></Property>
		<Property Name="Alarming:HiHi:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:HiHi:Description" Type="Str">Dewpoint is too high</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Limit" Type="Str">250.000000</Property>
		<Property Name="Alarming:HiHi:Name" Type="Str">HiHi</Property>
		<Property Name="Alarming:HiHi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Lo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Lo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Area" Type="Str"></Property>
		<Property Name="Alarming:Lo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:Lo:Description" Type="Str">Dewpoint is low</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Limit" Type="Str">25.000000</Property>
		<Property Name="Alarming:Lo:Name" Type="Str">Lo</Property>
		<Property Name="Alarming:Lo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str"></Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.010000</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Dewpoint is too low</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">10.000000</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Status:Area" Type="Str"></Property>
		<Property Name="Alarming:Status:Description" Type="Str"></Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Unit: K</Property>
		<Property Name="featurePacks" Type="Str">Alarming,Description,Logging,Network,Scaling</Property>
		<Property Name="Logging:Deadband" Type="Str">0.000000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.000100</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:URL" Type="Str">\\localhost\COMPACT-DAQ\DAQmxAI_AI0</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/TASCA/COMPACT.lvlib/</Property>
		<Property Name="Scaling:Coerce" Type="Str">False</Property>
		<Property Name="Scaling:EngMax" Type="Str">233.150000</Property>
		<Property Name="Scaling:EngMin" Type="Str">133.150000</Property>
		<Property Name="Scaling:EngUnit" Type="Str">Kelvin</Property>
		<Property Name="Scaling:RawMax" Type="Str">0.020000</Property>
		<Property Name="Scaling:RawMin" Type="Str">0.004000</Property>
		<Property Name="Scaling:Type" Type="Str">Linear</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Flow_0" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Description:Description" Type="Str">Unit: l/min</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.0001</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.0001</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Flow_1" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Description:Description" Type="Str">Unit: l/min</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.0001</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.0001</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Flow_2" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Description:Description" Type="Str">Unit: l/min</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.0001</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.0001</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Flow_3" Type="Variable">
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Description:Description" Type="Str">Unit: l/min</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.0001</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.0001</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MKS647C_ChannelStatus_0" Type="Variable">
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.0001</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">Refer to MKS647C manual</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">2</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">Hi</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="Description:Description" Type="Str">0: OK, Off
1: OK, On
&gt;1: Error</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Alarming,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.01</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.01</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&lt;&amp;Q!!!"=!A!!!!!!"!!5!"A!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MKS647C_ChannelStatus_1" Type="Variable">
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.0001</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">Refer to MKS647C manual</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">2</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">Hi</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="Description:Description" Type="Str">0: OK, Off
1: OK, On
&gt;1: Error</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Alarming,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.01</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.01</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&lt;&amp;Q!!!"=!A!!!!!!"!!5!"A!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MKS647C_ChannelStatus_2" Type="Variable">
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.0001</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">Refer to MKS647C manual</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">2</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">Hi</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="Description:Description" Type="Str">0: OK, Off
1: OK, On
&gt;1: Error</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Alarming,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.01</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.01</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&lt;&amp;Q!!!"=!A!!!!!!"!!5!"A!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="MKS647C_ChannelStatus_3" Type="Variable">
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.0001</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">Refer to MKS647C manual</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">2</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">Hi</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="Description:Description" Type="Str">0: OK, Off
1: OK, On
&gt;1: Error</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Alarming,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.01</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.01</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!&lt;&amp;Q!!!"=!A!!!!!!"!!5!"A!!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Pressure" Type="Variable">
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.0001</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">Pressure is high</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">600</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">HI</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:AckType" Type="Str">User</Property>
		<Property Name="Alarming:HiHi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:HiHi:Deadband" Type="Str">0.0001</Property>
		<Property Name="Alarming:HiHi:Description" Type="Str">Pressure is too high</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Limit" Type="Str">750</Property>
		<Property Name="Alarming:HiHi:Name" Type="Str">HIHI</Property>
		<Property Name="Alarming:HiHi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Lo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Lo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Lo:Deadband" Type="Str">0.0001</Property>
		<Property Name="Alarming:Lo:Description" Type="Str">Pressure is low</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Limit" Type="Str">300</Property>
		<Property Name="Alarming:Lo:Name" Type="Str">Lo</Property>
		<Property Name="Alarming:Lo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.0001</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Pressure is too low</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">100</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="Description:Description" Type="Str">Unit: hPa</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Alarming,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.0001</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.0001</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:URL" Type="Str">\\localhost\COMPACT-DAQ\DAQmxAI_AI1</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Temperature_0" Type="Variable">
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.0001</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">Temperature is high</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">293</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">Hi</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:AckType" Type="Str">User</Property>
		<Property Name="Alarming:HiHi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Area" Type="Str"></Property>
		<Property Name="Alarming:HiHi:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:HiHi:Description" Type="Str">Temperature is too high</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Limit" Type="Str">293</Property>
		<Property Name="Alarming:HiHi:Name" Type="Str">HiHi</Property>
		<Property Name="Alarming:HiHi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Lo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Lo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Area" Type="Str"></Property>
		<Property Name="Alarming:Lo:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:Lo:Description" Type="Str">Temperature is low</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Limit" Type="Str">25</Property>
		<Property Name="Alarming:Lo:Name" Type="Str">Lo</Property>
		<Property Name="Alarming:Lo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str"></Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Temperature too low</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">10</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Status:Area" Type="Str"></Property>
		<Property Name="Alarming:Status:Description" Type="Str"></Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Unit: K</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Alarming,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.0001</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.0001</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:URL" Type="Str">\\localhost\COMPACT-DAQ\DAQmxRTD_AI0</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/TASCA/COMPACT.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Temperature_1" Type="Variable">
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.0001</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">Temperature is high</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">250</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">Hi</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:AckType" Type="Str">User</Property>
		<Property Name="Alarming:HiHi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Area" Type="Str"></Property>
		<Property Name="Alarming:HiHi:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:HiHi:Description" Type="Str">Temperature is too high</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Limit" Type="Str">250</Property>
		<Property Name="Alarming:HiHi:Name" Type="Str">HiHi</Property>
		<Property Name="Alarming:HiHi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Lo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Lo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Area" Type="Str"></Property>
		<Property Name="Alarming:Lo:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:Lo:Description" Type="Str">Temperature is low</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Limit" Type="Str">25</Property>
		<Property Name="Alarming:Lo:Name" Type="Str">Lo</Property>
		<Property Name="Alarming:Lo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str"></Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Temperature too low</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">10</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Status:Area" Type="Str"></Property>
		<Property Name="Alarming:Status:Description" Type="Str"></Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Unit: K</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Alarming,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.0001</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.0001</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:URL" Type="Str">\\localhost\COMPACT-DAQ\DAQmxRTD_AI1</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/TASCA/COMPACT.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Temperature_2" Type="Variable">
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.0001</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">Temperature is high</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">200</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">Hi</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:AckType" Type="Str">User</Property>
		<Property Name="Alarming:HiHi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Area" Type="Str"></Property>
		<Property Name="Alarming:HiHi:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:HiHi:Description" Type="Str">Temperature is too high</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Limit" Type="Str">200</Property>
		<Property Name="Alarming:HiHi:Name" Type="Str">HiHi</Property>
		<Property Name="Alarming:HiHi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Lo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Lo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Area" Type="Str"></Property>
		<Property Name="Alarming:Lo:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:Lo:Description" Type="Str">Temperature is low</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Limit" Type="Str">25</Property>
		<Property Name="Alarming:Lo:Name" Type="Str">Lo</Property>
		<Property Name="Alarming:Lo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str"></Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Temperature too low</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">10</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Status:Area" Type="Str"></Property>
		<Property Name="Alarming:Status:Description" Type="Str"></Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Unit: K</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Alarming,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.0001</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.0001</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:URL" Type="Str">\\localhost\COMPACT-DAQ\DAQmxRTD_AI2</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/TASCA/COMPACT.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Temperature_3" Type="Variable">
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Hi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Hi:Deadband" Type="Str">0.0001</Property>
		<Property Name="Alarming:Hi:Description" Type="Str">Temperature is high</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Limit" Type="Str">173</Property>
		<Property Name="Alarming:Hi:Name" Type="Str">Hi</Property>
		<Property Name="Alarming:Hi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:HiHi:AckType" Type="Str">User</Property>
		<Property Name="Alarming:HiHi:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Area" Type="Str"></Property>
		<Property Name="Alarming:HiHi:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:HiHi:Description" Type="Str">Temperature is too high</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:HiHi:Limit" Type="Str">173</Property>
		<Property Name="Alarming:HiHi:Name" Type="Str">HiHi</Property>
		<Property Name="Alarming:HiHi:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Lo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Lo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Area" Type="Str"></Property>
		<Property Name="Alarming:Lo:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:Lo:Description" Type="Str">Temperature is low</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Lo:Limit" Type="Str">25</Property>
		<Property Name="Alarming:Lo:Name" Type="Str">Lo</Property>
		<Property Name="Alarming:Lo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:LoLo:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:LoLo:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Area" Type="Str"></Property>
		<Property Name="Alarming:LoLo:Deadband" Type="Str">0.01</Property>
		<Property Name="Alarming:LoLo:Description" Type="Str">Temperature too low</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:LoLo:Limit" Type="Str">10</Property>
		<Property Name="Alarming:LoLo:Name" Type="Str">LoLo</Property>
		<Property Name="Alarming:LoLo:Priority" Type="Str">1</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Status:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Status:Area" Type="Str"></Property>
		<Property Name="Alarming:Status:Description" Type="Str"></Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:Name" Type="Str">Status</Property>
		<Property Name="Alarming:Status:Priority" Type="Str">1</Property>
		<Property Name="Description:Description" Type="Str">Unit: K</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Alarming,Description</Property>
		<Property Name="Logging:Deadband" Type="Str">0.0001</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.0001</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:URL" Type="Str">\\localhost\COMPACT-DAQ\DAQmxRTD_AI3</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/UTCS.lvproj/My Computer/TASCA/COMPACT.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Valve_Set_0" Type="Variable">
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">High</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str"></Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Valve in bypass position</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.01</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.01</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:URL" Type="Str">\\localhost\COMPACT-DAQ\DAQmxDO_DO0</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Valve_Set_1" Type="Variable">
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">High</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str"></Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Valve in bypass position</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.01</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.01</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:URL" Type="Str">\\localhost\COMPACT-DAQ\DAQmxDO_DO1</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Valve_State_0_closed" Type="Variable">
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Valve in bypass position</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Alarming</Property>
		<Property Name="Logging:Deadband" Type="Str">0.01</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.01</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:URL" Type="Str">\\localhost\COMPACT-DAQ\DAQmxDI_DI0</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Valve_State_0_open" Type="Variable">
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Valve in bypass position</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="featurePacks" Type="Str">Logging,Network,Alarming</Property>
		<Property Name="Logging:Deadband" Type="Str">0.01</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.01</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:URL" Type="Str">\\localhost\COMPACT-DAQ\DAQmxDI_DI1</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!:&amp;1!!!"=!A!!!!!!"!!1!)1!"!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Valve_State_1_closed" Type="Variable">
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Valve in bypass position</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="featurePacks" Type="Str">Alarming,Logging,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:URL" Type="Str">\\localhost\COMPACT-DAQ\DAQmxDI_DI2</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Valve_State_1_open" Type="Variable">
		<Property Name="Alarming:BitArray:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Boolean:AckType" Type="Str">Auto</Property>
		<Property Name="Alarming:Boolean:AlarmOn" Type="Str">Low</Property>
		<Property Name="Alarming:Boolean:AllowLog" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Area" Type="Str">COMPACT</Property>
		<Property Name="Alarming:Boolean:Description" Type="Str">Valve in bypass position</Property>
		<Property Name="Alarming:Boolean:Enabled" Type="Str">True</Property>
		<Property Name="Alarming:Boolean:Name" Type="Str">Boolean</Property>
		<Property Name="Alarming:Boolean:Priority" Type="Str">1</Property>
		<Property Name="Alarming:Custom:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:EventOnDataChange" Type="Str">False</Property>
		<Property Name="Alarming:EventOnUserInputOnly" Type="Str">True</Property>
		<Property Name="Alarming:Hi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:HiHi:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Lo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:LoLo:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:ROC:Enabled" Type="Str">False</Property>
		<Property Name="Alarming:Status:Enabled" Type="Str">False</Property>
		<Property Name="featurePacks" Type="Str">Alarming,Logging,Network</Property>
		<Property Name="Logging:Deadband" Type="Str">0.010000</Property>
		<Property Name="Logging:LogData" Type="Str">True</Property>
		<Property Name="Logging:LogEvents" Type="Str">True</Property>
		<Property Name="Logging:LogFormat" Type="Str">String</Property>
		<Property Name="Logging:ValueRes" Type="Str">0.010000</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:SingleWriter" Type="Str">True</Property>
		<Property Name="Network:URL" Type="Str">\\localhost\COMPACT-DAQ\DAQmxDI_DI3</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>

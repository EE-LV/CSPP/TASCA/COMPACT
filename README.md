This submodule is used to develop the COMPACT part of the Unified TASCA Control System Project and is based on LVOOP, NI Actor Framework and CS++.

LabVIEW 2016 is currently used for development.

Related documents and information
=================================
- README.txt
- Release_Notes.txt
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de
- Download, bug reports... : http://github.com/HB-GSI/UTCS
- Documentation:
  - Refer to Documantation Folder
  - Project-Wikis: https://wiki.gsi.de/foswiki/bin/view/Tasca/UTCSP, https://github.com/HB-GSI/CSPP/wiki
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview

GIT Submodules
=================================
Following git submodules are defined in this project.
- Packages/CSPP_Core
- Packages/CSPP_DeviceBase
- Packages/CSPP_DSC: containing Alarm- & Trend-Viewer

External Dependencies
=================================
- BNT_DAQmx
- CSPP_MKS647C for
  - MKS6476C LabVIEW Instrument driver
Optional:


Author: H.Brand@gsi.de

Copyright 2017  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.